package com.webland.mybatis.pojo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Order {
    private Integer id;
    private Long userId;
    private String orderNumber;
    private Date created;
    private Date updated;
    //面向对象的思想，在Order对象中添加User对象
    private User user;
    public User getUser(){
        return user;
    }

    private List<OrderDetail> detailList;
    public List<OrderDetail> getDetailList(){ return detailList; }

    @Override
    public String toString() {
        return "Order{" +
                "id='" + id + '\'' +
                ", userId='" + userId.toString() + '\'' +
                ", orderNumber='" + orderNumber + '\'' +
                ", created='" + created + '\'' +
                ", updated='" + updated + '\'' +
                ", user=" + user + '\'' +
                '}';
    }
}
