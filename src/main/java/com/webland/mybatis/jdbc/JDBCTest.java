package com.webland.mybatis.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @author Daniel.John
 */
public class JDBCTest {
    public static void main(String[] args) throws Exception{
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;

        try {
            //加载驱动
            //Class.forName("com.mysql.jdbc.Driver");
            Class.forName("com.mysql.cj.jdbc.Driver");
            //获取连接
            String url = "jdbc:mysql://127.0.0.1:3306/ssmdemo?useUnicode=true&useSSL=false&characterEncoding=utf8&serverTimezone=GMT%2b8"; //serverTimezone=GMT%2b8
            String user = "root";
            String password = "r00t";
            connection = DriverManager.getConnection(url, user, password);
            //获取statement，preparedStatement
            String sql = " SELECT * FROM tb_user WHERE id LIKE ? ";
            preparedStatement = connection.prepareStatement(sql);
            //设置参数
            preparedStatement.setString(1, "%%");
            //执行查询
            rs  = preparedStatement.executeQuery();
            //处理结果
            while (rs.next()){
                System.out.println(rs.getString("user_Name"));
                System.out.println(rs.getString("name"));
                System.out.println(rs.getInt("age"));
                System.out.println(rs.getDate("birthday"));
            }
        } finally {
            //关闭连接，释放资源
            if(rs != null){
                rs.close();
            }
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(connection != null){
                connection.close();
            }
        }
    }
}
