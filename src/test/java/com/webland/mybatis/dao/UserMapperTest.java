package com.webland.mybatis.dao;

import com.webland.mybatis.dao.UserMapper;
import com.webland.mybatis.pojo.User;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class UserMapperTest {

    public UserMapper userMapper;
    SqlSession sqlSession = null;
    SqlSessionFactory sqlSessionFactory = null;

    @Before
    public void setUp() throws Exception {
        // 指定配置文件
        String resource = "mybatis-config.xml";
        // 读取配置文件
        InputStream inputStream = Resources.getResourceAsStream(resource);
        // 构建sqlSessionFactory
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取sqlSession
        sqlSession = sqlSessionFactory.openSession(true);

        // 1. 映射文件的命名空间（namespace）必须是mapper接口的全路径
        // 2. 映射文件的statement的id必须和mapper接口的方法名保持一致
        // 3. statement的resultType必须和mapper接口方法的返回类型一致
        // 4. statement的parameterType必须和mapper接口方法的参数类型一致（不一定）
        this.userMapper = sqlSession.getMapper(UserMapper.class);
    }

    @Test
    public void testLogin() {
        System.out.println(this.userMapper.login("hj", "123456"));
    }

    @Test
    public void testQueryUserByTableName() {
        List<User> userList = this.userMapper.queryUserByTableName("tb_user");
        for(User user : userList){
            System.out.println(user);
        }
    }

    @Test
    public void testQueryUserById() {
        System.out.println(this.userMapper.queryUserById("5"));
        //测试以及缓存
        //在mybatis中，一级缓存默认是开启的，并且一直无法关闭
        //1、同一个Session中
        //2、相同的SQL和参数

        //强制清除缓存
        //sqlSession.clearCache();
        //执行update、insert、delete的时候，会清空缓存
        User user = new User();
        user.setName("美女");
        user.setId("5");
        userMapper.updateUser(user);
        System.out.println(this.userMapper.queryUserById("5"));
    }

    //测试二级缓存
    //开启二级缓存，必须序列化
    @Test
    public void testCache(){
        System.out.println(this.userMapper.queryUserById("5"));
        sqlSession.close();
        sqlSession = sqlSessionFactory.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);

        System.out.println(mapper.queryUserById("5"));
    }

    @Test
    public void testQueryUserAll() {
        List<User> userList = this.userMapper.queryUserAll();
        for(User user: userList){
            System.out.println(user);
        }
    }

    @Test
    public void testQueryUserList(){
        List<User> userList = this.userMapper.queryUserList("静静");
        for(User user: userList){
            System.out.println(user);
        }
    }

    @Test
    public void testQueryUserListByNameOrAge() throws Exception{
        List<User> userList = this.userMapper.queryUserListByNameOrAge(null, 16);
        for(User user: userList){
            System.out.println(user);
        }
    }

    @Test
    public void testQueryUserListByNameAndAge() throws Exception{
        List<User> userList = this.userMapper.queryUserListByNameAndAge("大神", 20);
        for(User user: userList){
            System.out.println(user);
        }
    }

    @Test
    public void testQueryUserListByIds() throws Exception{
        List<User> users = this.userMapper.queryUserListByIds(new String[]{"1","2","3","5"});
        for (User user: users) {
            System.out.println(user);
        }
    }

    @Test
    public void testInsertUser() {
        User user = new User();
        user.setAge(20);
        user.setBirthday(new Date("1990/09/08"));
        user.setName("大神");
        user.setPassword("123321");
        user.setSex(2);
        user.setUserName("bigGod222");
        this.userMapper.insertUser(user);
        System.out.println(user.getId());
    }

    @Test
    public void testUpdateUser() {
        User user = new User();
        user.setBirthday(new Date("1991/12/02"));
        user.setName("我想静静");
        user.setPassword("123123");
        user.setSex(0);
        user.setUserName("IWJJ");
        user.setId("1");
        this.userMapper.updateUser(user);
    }

    @Test
    public void testDeleteUserById() {
        this.userMapper.deleteUserById("1");
    }
}