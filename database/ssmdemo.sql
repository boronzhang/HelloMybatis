/*
 Navicat Premium Data Transfer

 Source Server         : MySQL.Local
 Source Server Type    : MySQL
 Source Server Version : 50617
 Source Host           : localhost:3306
 Source Schema         : ssmdemo

 Target Server Type    : MySQL
 Target Server Version : 50617
 File Encoding         : 65001

 Date: 04/03/2021 23:37:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_item
-- ----------------------------
DROP TABLE IF EXISTS `tb_item`;
CREATE TABLE `tb_item`  (
  `id` int(11) NOT NULL,
  `itemName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `itemPrice` decimal(10, 2) NULL DEFAULT NULL,
  `itemDetail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_item
-- ----------------------------
INSERT INTO `tb_item` VALUES (1, '袜子', 29.90, '香香的袜子');
INSERT INTO `tb_item` VALUES (2, '套子', 99.99, '冈本001');

-- ----------------------------
-- Table structure for tb_order
-- ----------------------------
DROP TABLE IF EXISTS `tb_order`;
CREATE TABLE `tb_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `order_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `updated` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_order
-- ----------------------------
INSERT INTO `tb_order` VALUES (1, 2, '201807010001', '2018-07-01 19:38:35', '2018-07-01 19:38:40');

-- ----------------------------
-- Table structure for tb_orderdetail
-- ----------------------------
DROP TABLE IF EXISTS `tb_orderdetail`;
CREATE TABLE `tb_orderdetail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NULL DEFAULT NULL,
  `total_price` decimal(10, 0) NULL DEFAULT NULL,
  `item_id` int(11) NULL DEFAULT NULL,
  `status` int(10) UNSIGNED ZEROFILL NULL DEFAULT NULL COMMENT '0成功非0失败',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_orderdetail
-- ----------------------------
INSERT INTO `tb_orderdetail` VALUES (1, 1, 10000, 1, 0000000001);
INSERT INTO `tb_orderdetail` VALUES (2, 1, 2000, 2, 0000000000);

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `age` int(10) NULL DEFAULT NULL,
  `sex` int(2) NULL DEFAULT NULL,
  `birthday` date NULL DEFAULT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `updated` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('10', 'bigGod222', '123321', '大神', 20, 2, '1990-09-07', '2021-02-22 16:14:20', '2021-02-22 16:14:20');
INSERT INTO `tb_user` VALUES ('11', 'bigGod222', '123321', '大神', 20, 2, '1990-09-07', '2021-02-22 16:15:38', '2021-02-22 16:15:38');
INSERT INTO `tb_user` VALUES ('12', 'bigGod222', '123321', '大神', 20, 2, '1990-09-07', '2021-02-22 16:59:06', '2021-02-22 16:59:06');
INSERT INTO `tb_user` VALUES ('13', 'bigGod222', '123321', '大神', 20, 2, '1990-09-07', '2021-02-22 17:18:07', '2021-02-22 17:18:07');
INSERT INTO `tb_user` VALUES ('14', 'bigGod222', '123321', '大神', 20, 2, '1990-09-07', '2021-03-03 15:49:27', '2021-03-03 15:49:27');
INSERT INTO `tb_user` VALUES ('2', 'hj', '123456', '静静', 22, 1, '1993-09-05', '2021-01-08 15:06:38', '2021-01-08 15:06:38');
INSERT INTO `tb_user` VALUES ('3', 'evan', '123456', '大鹏', 16, 1, '1990-09-01', '2021-02-19 10:52:54', '2021-02-19 10:52:54');
INSERT INTO `tb_user` VALUES ('5', 'evan', '123456', '大鹏', 16, 1, '1990-09-01', '2021-02-19 10:58:25', '2021-02-19 10:58:25');
INSERT INTO `tb_user` VALUES ('6', 'evan', '123456', '大鹏', 16, 1, '1990-09-01', '2021-02-19 10:59:03', '2021-02-19 10:59:03');
INSERT INTO `tb_user` VALUES ('7', 'evan', '123456', '大鹏', 16, 1, '1990-09-01', '2021-02-19 17:13:10', '2021-02-19 17:13:10');
INSERT INTO `tb_user` VALUES ('8', 'bigGod222', '123321', '大神', 20, 2, '1990-09-07', '2021-02-20 15:25:34', '2021-02-20 15:25:34');
INSERT INTO `tb_user` VALUES ('9', 'bigGod222', '123321', '大神', 20, 2, '1990-09-07', '2021-02-22 16:09:04', '2021-02-22 16:09:04');

SET FOREIGN_KEY_CHECKS = 1;
